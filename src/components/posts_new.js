import _ from 'lodash';
import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Link} from 'react-router';

import {createPost} from '../actions/index';

const FIELDS = {
    title: {
        type: 'input',
        label: 'Title for post'
    },
    categories: {
        type: 'input',
        label: 'Enter some categories for this post'
    },
    content: {
        type: 'textarea',
        label: 'Post Contents'
    }
};

class PostsNew extends Component {
    // To programatically navigate in the app without link, we need access to react router
    // To get access need to define contextType, which gives access to a property from parent component
    static contextTypes = {
        router: PropTypes.object
    };

    // props here is properties from the form, not this.props
    onSubmit(props) {
        this.props.createPost(props)
            .then(() => {
                //blog post has been created, navigate the user to the index
                this.context.router.push('/');
            });
    }

    renderField(fieldConfig, field) {
        // fieldHelper provided by redux-form, it's a object
        const fieldHelper = this.props.fields[field];

        return (
            <div className={`form-group ${fieldHelper.touched && fieldHelper.invalid ? 'has-danger' : ''}`} key={fieldConfig.label}>
                <label>{fieldConfig.label}</label>
                <fieldConfig.type type="text" className="form-control" {...fieldHelper} />
                <div className="form-control-label">{fieldHelper.touched ? fieldHelper.error : ''}</div>
            </div>
        );
    }

    render() {
        // const title = this.props.fields.title; es05 v es06
        const {handleSubmit} = this.props;

        // Destructuring of object to different keys and values
        // {...title}

        return (
            <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) }>
                <h3>Create a New Post</h3>

                {_.map(FIELDS, this.renderField.bind(this))}

                <button type="submit" className="btn btn-primary">Submit</button>
                <Link to="/" className="btn btn-danger">Cancel</Link>
            </form>
        );
    }
}

function validate(values) {
    const errors = {};

    _.each(FIELDS, (type, field) => {
        if (!values[field]) {
            errors[field] = `Enter a ${field}`;
        }
    });

    return errors;
}

// connect: 1st argument is mapStateToProps, 2nd is mapDispatchToProps
// reduxForm: 1st is form config, 2nd is mapStateToProps, 3rd is mapDispatchToProps

export default reduxForm({
    form: 'PostsNewForm',
    fields: _.keys(FIELDS),
    validate
}, null, {createPost})(PostsNew);